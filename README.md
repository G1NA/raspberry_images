==============================================================

This image file has been preinstalled with all the software used for the experiments conducted during the UNSURPASSED project. It can be directly written in an SD card and used in a Raspberry Pi 3 B, thus saving time from having to install the drivers for the wireless cards and all other requirements.

The image file contains the following:

* OS: Ubuntu Mate 16.04

* The latest versions of the following (as of 22/11/18)
    - babeld
    - bmx7
    - batman
    - ibr-dtn
    - ccn-lite
    - Dedalus, preinstalled on the home directory.

Note: the name of the computer within the image should be changed, as well as the IP address of the ad hoc interface in order to have a unique node.

A list of useful files that you might want to edit include:

* /etc/init.d/dedalus - Contains the starting and closing script for the Dedalus software and it is being run at startup.

* /etc/network/interfaces - Edit this file to configure the interfaces of the node. We presuppose the use of two network interfaces, one for ad hoc connectivity and one acting as a control network. The control network is optional, though, and you could use the ad hoc interface itself for passing the various Dedalus control messages.

* /etc/systemd/network/10-internet.link and  20-internet.link - These files will alter the name of each interface for convenience reasons. The MAC address on each machine will be different, though, and as such will have to be updated based on the output of the `ifconfig` command.

Logging is done in the following files:

* ~/dedalus.log

* ~/Dedalus/dedalus/Log (each sub-module will log in a separate file, errors.log and dedalus-%date%.log is a rotating log file)